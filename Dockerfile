FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

MAINTAINER Authors name <support@cloudron.io>

RUN mkdir -p /app/code /run/piwigo

WORKDIR /app/code

ENV PIWIGOVERSION=2.9.5

# configure apache
RUN rm /etc/apache2/sites-enabled/* && \
    sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf

RUN a2disconf other-vhosts-access-log

# configure mod_php
RUN a2enmod php7.2 rewrite
RUN crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_filesize 128M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_size 128M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP post_max_size 128M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.save_path /run/piwigo/sessions && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_divisor 100

# install exiftools and jpegtran
RUN apt update && apt-get install -y libimage-exiftool-perl libjpeg-progs

# get piwigo
RUN wget -O piwigo-${PIWIGOVERSION}.zip https://piwigo.org/download/dlcounter.php?code=${PIWIGOVERSION} && \
    unzip piwigo-${PIWIGOVERSION}.zip && \
    rm piwigo-${PIWIGOVERSION}.zip

# move folders around
RUN mv /app/code/piwigo/_data /app/code/piwigo/_data_old && \
    mv /app/code/piwigo/plugins /app/code/piwigo/_plugins && \
    mv /app/code/piwigo/themes /app/code/piwigo/_themes && \
    mv /app/code/piwigo/language /app/code/piwigo/_language && \
    mv /app/code/piwigo/local /app/code/piwigo/_local && \
    mv /app/code/piwigo/* /app/code/ && \
    rm -R /app/code/piwigo/ /app/code/upload && \
    mv /app/code/include/ws_functions/pwg.categories.php /app/code/include/ws_functions/pwg.categories_orig.php
 
# create folders for localstorage
# RUN mkdir -p /app/data/plugins /app/data/_data/ /app/data/upload/ /app/data/config

# link to static directories
RUN ln -sf /app/data/plugins /app/code/plugins && \
    ln -sf /app/data/_data /app/code/_data && \
    ln -sf /app/data/language /app/code/language && \
    ln -sf /app/data/themes /app/code/themes && \
    ln -sf /app/data/local/ /app/code/local && \
    ln -sf /app/data/upload /app/code/upload && \
    ln -sf /app/data/pwg.categories.php /app/code/include/ws_functions/pwg.categories.php

RUN chown -R www-data.www-data /app/code

ADD piwigo.conf /etc/apache2/sites-enabled/piwigo.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

ADD start.sh /app/

CMD [ "/app/start.sh" ]
