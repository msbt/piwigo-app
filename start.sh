#!/bin/bash

set -eu

# setup waits for apache to start and configures piwigo
setup() {

    if [[ ! -f /app/data/_data/dummy.txt ]]; then
        echo "=> Detected first run"

        mkdir -p /app/data/_data /app/data/upload /app/data/plugins /app/data/local/config /app/data/language /app/data/themes
        chmod -R 0777 /app/data/_data /app/data/local /app/data/themes

        # copy stuff
        cp -r /app/code/_plugins/* /app/data/plugins/
        cp -r /app/code/_themes/* /app/data/themes/
        cp -r /app/code/_language/* /app/data/language/
        cp -r /app/code/_local/* /app/data/local/
        cp /app/code/_data_old/dummy.txt /app/data/_data/
        cp /app/code/include/ws_functions/pwg.categories_orig.php /app/data/pwg.categories.php
    fi


    while [[ ! -f "/var/run/apache2/apache2.pid" ]]; do
        echo "Waiting for apache2 to start"
        sleep 3
    done

    # check if config is there, if not, install piwigo
    if [[ ! -f /app/data/local/config/database.inc.php ]]; then
        curl -L -X POST --data "?language=en_UK&install=true&dbhost=${MYSQL_HOST}&dbuser=${MYSQL_USERNAME}&dbpasswd=${MYSQL_PASSWORD}&dbname=${MYSQL_DATABASE}&admin_name=admin&admin_pass1=changeme&admin_pass2=changeme&admin_mail=admin@${APP_DOMAIN}" "http://localhost:8000/install.php"

    fi

    # install ldap plugin
    if [[ ! -d /app/data/plugins/ldap_login ]]; then
        wget -O /tmp/Ldap_Login.zip https://github.com/kvakanet/ldap_login/archive/config2base.zip
        unzip /tmp/Ldap_Login.zip -d /app/data/plugins/ldap_login
        rm /tmp/Ldap_Login.zip
        mv /app/data/plugins/ldap_login/ldap_login-config2base/* /app/data/plugins/ldap_login
        rm -R /app/data/plugins/ldap_login/ldap_login-config2base/

        # activate plugin in db
        mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE} <<< "INSERT INTO plugins (id,state,version) VALUES ('ldap_login','active','1.2');"
        # add ldap infos in db
        mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE} <<< "INSERT INTO config (param,value) VALUES ('ldap_login_plugin','a:18:{s:4:\"host\";s:10:\"172.18.0.1\";s:6:\"basedn\";s:11:\"dc=cloudron\";s:11:\"usersbranch\";s:8:\"ou=users\";s:11:\"groupbranch\";s:21:\"ou=groups,dc=cloudron\";s:4:\"port\";s:4:\"3002\";s:7:\"ld_attr\";s:8:\"username\";s:8:\"ld_group\";s:2:\"cn\";s:9:\"ld_binddn\";s:0:\"\";s:9:\"ld_bindpw\";s:0:\"\";s:16:\"webmasters_group\";s:6:\"admins\";s:12:\"admins_group\";s:5:\"users\";s:10:\"ld_use_ssl\";b:0;s:16:\"ld_search_groups\";b:0;s:15:\"ld_search_users\";b:0;s:11:\"users_group\";s:0:\"\";s:14:\"allow_newusers\";b:1;s:28:\"advertise_admin_new_ldapuser\";b:0;s:26:\"send_password_by_mail_ldap\";b:0;}');"
    fi


    # update user and pass in case they changed
    if [[ -f /app/data/local/config/database.inc.php ]]; then
        sed -i "s/\$conf\['db_base'\] = .*/\$conf\['db_base'\] = \'${MYSQL_DATABASE}\';/" /app/data/local/config/database.inc.php
        sed -i "s/\$conf\['db_user'\] = .*/\$conf\['db_user'\] = \'${MYSQL_USERNAME}\';/" /app/data/local/config/database.inc.php
        sed -i "s/\$conf\['db_password'\] = .*/\$conf\['db_password'\] = \'${MYSQL_PASSWORD}\';/" /app/data/local/config/database.inc.php
        sed -i "s/\$conf\['db_host'\] = .*/\$conf\['db_host'\] = \'${MYSQL_HOST}\';/" /app/data/local/config/database.inc.php
    fi

    # enforce ssl
    if [[ ! -f /app/data/local/config/config.inc.php ]]; then
        touch /app/data/local/config/config.inc.php
    fi

    if ! grep -q HTTP_X_FORWARDED_FOR "/app/data/local/config/config.inc.php"; then
        cat << 'EOT' > /app/data/local/config/config.inc.php
<?php
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
$_SERVER['HTTP_HOST'] = $_SERVER['HTTP_X_FORWARDED_HOST'];
if ($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
$_SERVER['HTTPS']='on';
$conf['send_bcc_mail_webmaster'] = false;
$conf['mail_sender_name'] = '';
$conf['mail_sender_email'] = '${MAIL_FROM}';
$conf['mail_allow_html'] = true;
$conf['smtp_host'] = '${MAIL_SMTP_SERVER}:${MAIL_SMTP_PORT}';
$conf['smtp_user'] = '${MAIL_SMTP_USERNAME}';
$conf['smtp_password'] = '${MAIL_SMTP_PASSWORD}';
$conf['smtp_secure'] = null;
}?>
EOT
    fi

    # update email data in case of changes
    if [[ -f /app/data/local/config/config.inc.php ]]; then
        sed -i "s/\$conf\['smtp_host'\] = .*/\$conf\['smtp_host'\] = \'${MAIL_SMTP_SERVER}:${MAIL_SMTP_PORT}\';/" /app/data/local/config/config.inc.php
        sed -i "s/\$conf\['smtp_user'\] = .*/\$conf\['smtp_user'\] = \'${MAIL_SMTP_USERNAME}\';/" /app/data/local/config/config.inc.php
        sed -i "s/\$conf\['smtp_password'\] = .*/\$conf\['smtp_password'\] = \'${MAIL_SMTP_PASSWORD}\';/" /app/data/local/config/config.inc.php
        sed -i "s/\$conf\['mail_sender_email'\] = .*/\$conf\['mail_sender_email'\] = \'${MAIL_FROM}\';/" /app/data/local/config/config.inc.php
    fi


    chown -R www-data.www-data /app/data /run
}

( setup ) &

echo "=> Run piwigo"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"

exec /usr/sbin/apache2 -DFOREGROUND
