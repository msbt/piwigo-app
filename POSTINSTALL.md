This application integrates with cloudron. However, here's an admin account, all LDAP users are regular users and are only able to view images by default (which can be changed in the config)

* User: `admin`
* Pass: `changeme`

**Please change the credentials immediately**
